" Load Plugins
call plug#begin('~/.vim/plugged')
	Plug 'preservim/nerdtree'		" File Tree
	Plug 'vim-airline/vim-airline'		" Info Bar 
	Plug 'vim-airline/vim-airline-themes'	" Bar Themes
	Plug 'airblade/vim-gitgutter' 		" Git Info	
call plug#end()

" Airline Settings 
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='base16_mocha'

" NERDTree Settings
nnoremap <C-n> :NERDTreeMirror<CR>:NERDTreeFocus<CR>
autocmd BufEnter * if tabpagenr('$') == 1 && winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree() | quit | endif

" General Settings 
set number





