#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
#PS1='[\u@\h \W]\$ '
source /usr/share/nvm/init-nvm.sh
alias dotfiles='/usr/bin/git --git-dir=/home/.dotfiles/ --work-tree=/home'

alias cutefetch='CF_ITALIC=true ./cutefetch.sh owl'
export PS1="\e[3m \w \e[0m » "
